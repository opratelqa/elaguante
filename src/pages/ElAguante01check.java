package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class ElAguante01check extends TestBaseTG {
	
	final WebDriver driver;
	public ElAguante01check(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	
	@FindBy(how = How.ID,using = "search-3")
	private WebElement titulo;
	
	@FindBy(how = How.ID,using = "search-3")
	private WebElement titulo2;
	
	@FindBy(how = How.ID,using = "search-3")
	private WebElement titulo3;
	
	@FindBy(how = How.ID,using = "search-3")
	private WebElement titulo4;
	
	//@FindBy(how = How.ID,using = "login")
	//private WebElement titulo5;
	
	@FindBy(how = How.XPATH,using = "//*[@id=\"login\"]")
	private WebElement titulo5;
	
	//*****************************

	
	public void logInElAguante(String apuntaA) {
		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test El Aguante 01 - Check");
		

	WebElement Home = driver.findElement(By.xpath("//a[contains(text(), 'la celeste')]"));
	Home.click();
	
	String Titulo = titulo.getText();
		Assert.assertEquals("BUSCAR", Titulo);
		System.out.println(Titulo);

	espera(900);
	
  WebElement internacionales = driver.findElement(By.xpath("//a[contains(text(), 'internacionales')]"));
	internacionales.click();
	
	String Titulo2 = titulo2.getText();
	Assert.assertEquals("BUSCAR", Titulo2);
	System.out.println(Titulo2);
	
	espera(900);
	
	WebElement Peticiones = driver.findElement(By.xpath("//a[contains(text(), 'hecho en casa')]"));
	Peticiones.click();
	
	String Titulo3 = titulo3.getText();
	Assert.assertEquals("BUSCAR", Titulo3);
	System.out.println(Titulo3);
	
	espera(900);
	
	WebElement PildorasDeFe = driver.findElement(By.xpath("//a[contains(text(), 'El rincón de las arañas')]"));
	PildorasDeFe.click();
	
	String Titulo4 = titulo4.getText();
	Assert.assertEquals("BUSCAR", Titulo4);
	System.out.println(Titulo4);
	
	espera(900);
	
	WebElement Adoptame = driver.findElement(By.xpath("//a[contains(text(), 'suscripción')]"));
	Adoptame.click();
	
	
	//valida que los menus se vean correctamente
	
			
	System.out.println("Resultado Esperado:Deberan visualizarce las secciones de El Aguante"); 
	System.out.println();
	System.out.println("Fin de Test El Aguante 01 - Check");
		

	}		

}  

